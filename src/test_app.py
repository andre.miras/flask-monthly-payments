import os

import tempfile

import pytest

from app import app, compute_monthly_payment


@pytest.fixture
def client():
    return app.test_client()


def test_index(client):
    """Checks the landing page."""
    response = client.get("/")
    assert response.status_code == 200
    assert b"Monthly payment calculator" in response.data


def test_form_post(client):
    data = {
        "amount": 200000,
        "term": 20,
        "interest": 1.75,
    }
    response = client.post("/", data=data, follow_redirects=True)
    assert b"Monthly payment: 988.26" in response.data
    assert b"Loan amount: 200000" in response.data
    assert b"Interest rate: 1.75%" in response.data
    assert b"Terms: 20 years" in response.data


def test_compute_monthly_payment():
    amount = 200000
    term = 20
    interest = 1.75
    assert compute_monthly_payment(amount, term, interest) == 988.26
