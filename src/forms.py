from wtforms import DecimalField, Form, IntegerField

default_render_kw = {"class": "form-control"}


class LoanForm(Form):
    amount = IntegerField(
        "Loan amount", render_kw={**default_render_kw, "placeholder": "150000"}
    )
    term = IntegerField(
        "Loan term (in years)", render_kw={**default_render_kw, "placeholder": "20"}
    )
    interest = DecimalField(
        "Interest", render_kw={**default_render_kw, "placeholder": "1.75"}
    )
