from flask import Flask, render_template, request

from forms import LoanForm

app = Flask(__name__)


def compute_monthly_payment(amount, term, interest):
    """
    https://fr.wikipedia.org/wiki/Mensualit%C3%A9#Calcul_formel
    """
    interest /= 100
    monthly_payment = (amount * interest / 12) / (
        1 - (1 / (pow((1 + (interest / 12)), term * 12)))
    )
    return round(monthly_payment, 2)


@app.route("/", methods=["GET", "POST"])
def index():
    monthly_payment = None
    form = LoanForm(request.form)
    if request.method == "POST" and form.validate():
        monthly_payment = compute_monthly_payment(
            form.amount.data, form.term.data, form.interest.data
        )
    context = {"form": form, "monthly_payment": monthly_payment}
    return render_template("index.html", **context)
