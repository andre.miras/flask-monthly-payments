# Flask Monthly Payments

Simple loan calculator demo using Flask.


## Install & Run
```sh
pip install -r requirements.txt
FLASK_ENV=development FLASK_APP=src/app.py flask run
```
